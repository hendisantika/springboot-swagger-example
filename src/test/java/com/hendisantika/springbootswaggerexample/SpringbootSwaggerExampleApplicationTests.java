package com.hendisantika.springbootswaggerexample;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SpringbootSwaggerExampleApplicationTests {

    @Test
    @Disabled("Disabled until CustomerService is up!")
	public void contextLoads() {
	}

}
