package com.hendisantika.springbootswaggerexample.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/12/17
 * Time: 7:01 AM
 * To change this template use File | Settings | File Templates.
 */

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.hendisantika.springbootswaggerexample.domain"})
@EnableJpaRepositories(basePackages = {"com.hendisantika.springbootswaggerexample.repositories"})
@EnableTransactionManagement
public class RepositoryConfiguration {
}
