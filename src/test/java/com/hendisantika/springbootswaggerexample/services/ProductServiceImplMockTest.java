package com.hendisantika.springbootswaggerexample.services;

import com.hendisantika.springbootswaggerexample.domain.Product;
import com.hendisantika.springbootswaggerexample.repositories.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/12/17
 * Time: 7:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class ProductServiceImplMockTest {
    private ProductServiceImpl productServiceImpl;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private Product product;

    @BeforeEach
    public void setupMock() {
        MockitoAnnotations.initMocks(this);
        productServiceImpl = new ProductServiceImpl();
        productServiceImpl.setProductRepository(productRepository);
    }

    @Test
    public void shouldReturnProduct_whenGetProductByIdIsCalled() throws Exception {
        // Arrange
        when(productRepository.findById(5)).thenReturn(java.util.Optional.of(product));
        // Act
        Product retrievedProduct = productServiceImpl.getProductById(5);
        // Assert
        assertThat(retrievedProduct, is(equalTo(product)));

    }

    @Test
    public void shouldReturnProduct_whenSaveProductIsCalled() throws Exception {
        // Arrange
        when(productRepository.save(product)).thenReturn(product);
        // Act
        Product savedProduct = productServiceImpl.saveProduct(product);
        // Assert
        assertThat(savedProduct, is(equalTo(product)));
    }

    @Test
    public void shouldCallDeleteMethodOfProductRepository_whenDeleteProductIsCalled() throws Exception {
        // Arrange
        doNothing().when(productRepository).deleteById(5);
        ProductRepository my = Mockito.mock(ProductRepository.class);
        // Act
        productServiceImpl.deleteProduct(5);
        // Assert
        verify(productRepository, times(1)).deleteById(5);
    }
}
