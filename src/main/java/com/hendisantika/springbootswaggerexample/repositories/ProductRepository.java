package com.hendisantika.springbootswaggerexample.repositories;

import com.hendisantika.springbootswaggerexample.domain.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/12/17
 * Time: 6:32 AM
 * To change this template use File | Settings | File Templates.
 */
@RepositoryRestResource
public interface ProductRepository extends CrudRepository<Product, Integer> {
}
