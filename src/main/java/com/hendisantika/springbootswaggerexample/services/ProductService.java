package com.hendisantika.springbootswaggerexample.services;

import com.hendisantika.springbootswaggerexample.domain.Product;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/12/17
 * Time: 6:39 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ProductService {
    Iterable<Product> listAllProducts();

    Product getProductById(Integer id);

    Product saveProduct(Product product);

    void deleteProduct(Integer id);
}
