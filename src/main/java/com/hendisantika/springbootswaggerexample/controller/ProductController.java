package com.hendisantika.springbootswaggerexample.controller;

import com.hendisantika.springbootswaggerexample.domain.Product;
import com.hendisantika.springbootswaggerexample.services.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/12/17
 * Time: 6:57 AM
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/products")
@Tag(name = "Online Store", description = "Operations pertaining to products in Online Store")
public class ProductController {
    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Operation(
            summary = "View a list of available products",
            description = "View a list of available products.",
            tags = {"Online Store"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Iterable.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "You are not authorized to view the " +
                    "resource", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Accessing the resource you were " +
                    "trying to reach is forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "The resource you were trying to reach" +
                    " is not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    @GetMapping(value = "/list")
    public Iterable<Product> list(Model model) {
        Iterable<Product> productList = productService.listAllProducts();
        return productList;
    }

    @GetMapping(value = "/show/{id}")
    @Operation(
            summary = "Search a product with an ID",
            description = "Search a product with an ID.",
            tags = {"Online Store"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "You are not authorized to view the " +
                    "resource", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Accessing the resource you were " +
                    "trying to reach is forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "The resource you were trying to reach" +
                    " is not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public Product showProduct(@PathVariable Integer id, Model model) {
        Product product = productService.getProductById(id);
        return product;
    }

    @PostMapping(value = "/add")
    @Operation(
            summary = "Add a product",
            description = "Add a product.",
            tags = {"Online Store"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "You are not authorized to view the " +
                    "resource", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Accessing the resource you were " +
                    "trying to reach is forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "The resource you were trying to reach" +
                    " is not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity saveProduct(@RequestBody Product product) {
        productService.saveProduct(product);
        return new ResponseEntity("Product saved successfully", HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}")
    @Operation(
            summary = "Update a product",
            description = "Update a product.",
            tags = {"Online Store"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "You are not authorized to view the " +
                    "resource", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Accessing the resource you were " +
                    "trying to reach is forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "The resource you were trying to reach" +
                    " is not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity updateProduct(@PathVariable Integer id, @RequestBody Product product) {
        Product storedProduct = productService.getProductById(id);
        storedProduct.setDescription(product.getDescription());
        storedProduct.setImageUrl(product.getImageUrl());
        storedProduct.setPrice(product.getPrice());
        productService.saveProduct(storedProduct);
        return new ResponseEntity("Product updated successfully", HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    @Operation(
            summary = "Delete a product",
            description = "Delete a product.",
            tags = {"Online Store"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "You are not authorized to view the " +
                    "resource", responseCode = "401",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Accessing the resource you were " +
                    "trying to reach is forbidden", responseCode = "403",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "The resource you were trying to reach" +
                    " is not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public ResponseEntity delete(@PathVariable Integer id) {
        productService.deleteProduct(id);
        return new ResponseEntity("Product deleted successfully", HttpStatus.OK);

    }
}
