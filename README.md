# Spring Boot Swagger Example

### Rest Service Documentation using Swagger

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-swagger-example.git`
2. Navigate to the folder: `cd springboot-swagger-example`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080/swagger-ui

### Image Screen shot

Login Page

![Login Page](img/signin.png "Login Page")

Swagger UI Page

![Swagger UI Page](img/swagger.png "Swagger UI Page")
